#lang racket

;; REMOVE MEMBER Version1 and REMBER

;;using define procedure to create a new function called remberV1
(define remberV1
  ;;lambda creates new anonymous function ,which is evaluated each time I use it
  (lambda (a lat)
    ;;cond statement
    (cond
      ;; first condition
      ((null? lat) (quote () ))
      (else (cond
              ;; second condition on else side
              ;; if condition true, execute statement
             (( eq? (car lat ) a) (cdr lat))
             ;;recalling the function --> recursion
             (else (remberV1 a
                           (cdr lat))))))))

;; rember Version1 testing
( remberV1 'and '(lettuce bacon and tomato))

;;it's not a good version yet because remove everything what's before the searched member


(define rember
  (lambda (a lat)
    (cond
      ((null? lat) (quote () ))           
      (( eq? (car lat ) a) (cdr lat))
      ;; perservers the members of the list by using cons
      ;; recalling the function for unverified atoms
      (else ( cons (car lat) (rember a (cdr lat)))))))

;; rember testing
( rember 'and '(lettuce bacon and tomato))
;; now it's better because only deletes the searched element

;; FIRSTS
(define firsts
  (lambda (l)
    (cond
      (( null? l) '() )
      (else (cons (car (car l))
                  (firsts (cdr l)))))))
;; firsts testing
;;(firsts '((uno due tre quattro) (due tre quattro) (tre quattro) (quattro)))

;; I need atom? to improve firsts

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

 (define firsts2
   (lambda (l)
     (cond
       ((null? l) '())
       ((atom?(car l))(firsts2 (cdr l)))
       (else (cons (car (car l)) (firsts2 (cdr l)))))))

(firsts2 '((uno due tre quattro) 'atom (due tre quattro) (tre quattro) (quattro)))

;; SUBSTS

( define subst
   (lambda (new old lat)
     ;;cond statement
     (cond
       ;;first condition
       ((null? lat) (quote ()))
       ;; else condition & cond statement
       (else (cond
               ;; condition
               (( eq? (car lat) old)
                ;; result evaluated-when-true condition
                (cons new (cdr lat)))
               ;; else condtion
               (else (cons (car lat)
                           ;; recursion
                           (subst new old
                                  (cdr lat)))))))))



;; subst testing
(subst 'a 'b '(b b c))

;; tracing execution

;; (subst 'a 'd '(e b c))
;; (cons 'e (subst 'a 'd '(b c)))
;; (cons 'e (cons 'b ( subst 'a 'd '(c))))
;; (cons 'e (cons 'b (( cons 'c ( subst 'a 'd '())))))
;; (cons 'e (cons 'b (cons 'c '())))
;; (cons 'e ( cons 'b '(c)))
;; (cons 'e '(b c))
;; '(e b c)


;;INSERT RIGHT

(define insertR
  (lambda (new old lat)
    (cond
      ;; first condition
      (( null? lat) ('()))
      (else (cond
              ;; condition
              ((eq? (car lat) old)
                ;; statement executed when condition-true
               (cons old (cons new (cdr lat))))
              ;; else & condition 
              (else (cons ( car lat)
                          ;; recalling the function -->recursion
                          (insertR new old
                                  (cdr lat)))))))))

;; insertR testing
(insertR 'topping 'fudge '( ice cream with fudge for dessert))

;; INSERT LEFT

(define insertL
  ;; creates a functions with 3 parametres
  (lambda (new old lat)
    ;;cond statement 
    ( cond
       ;; first condition ( closed-loop condition)
       ((null? lat) ('()))
       (else
          (cond
            ;;condition 
            ((eq? (car lat) old )
            ;; statement executed when condition true
             (cons new
                   (cons old (cdr lat))))
            ;;else statement
            (else
             ;; otherwise, keep the atoms  
             (cons (car lat)
                        ;; recalling the function -->recursion
                        (insertL new old
                                 (cdr lat)))))))))
;; insertL testing 
 ( insertL 'topping 'fudge '(ice cream with fudge for desert))


;;MULTI
'(MULTI REMBER)

(define multirember
  (λ (a lat)
    (cond
      ((null? lat)(quote()))
      (else
       (cond
         ((eq? (car lat) a)
          (multirember a (cdr lat)))
         (else (cons (car lat)
                     (multirember a
                                  (cdr lat)))))))))

(multirember 'm '(m a m a m i a))

'(MULTI INSERT RIGHT)

(define multiinsertR
  (λ (new old lat)
    (cond
      ((null? lat)(quote()))
      (else
       (cond
         ((eq? (car lat) old)
          (cons (car lat) 
                (cons new
                      (multiinsertR new old
                                    (cdr lat)))))
         (else (cons (car lat)
                     (multiinsertR new old
                                   (cdr lat)))))))))

(multiinsertR 'a 't '(t t))


'(MULTI INSERT LEFT)
(define multiinsertL
  (λ (new old lat)
    (cond
      ((null? lat)(quote()))
      (else
       (cond
         ((eq? (car lat) old)
          (cons new
                (cons old
                      (multiinsertL new old
                                    (cdr lat)))))
         (else (cons (car lat)
                     (multiinsertL new old
                                   (cdr lat)))))))))

(multiinsertL 'm 'a '( a a))


'(MULTI SUBST)

(define multisubst
  (λ (new old lat)
    (cond
      ((null? lat)(quote()))
      (else (cond
              ((eq? (car lat) old)
               (cons new
                     (multisubst new old
                                 (cdr lat))))
              (else (cons (car lat)
                          (multisubst new old
                                      (cdr lat)))))))))

(multisubst 'ole 'waai '(waai waai ole ole wai))
(quote())

(cdr '(a ()))