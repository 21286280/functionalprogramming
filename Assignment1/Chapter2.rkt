#lang racket

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))
'lat?
;; LAT?

(define lat?
  (lambda (l)
    (cond
      (( null? l) #t )
      ((atom? (car l)) (lat? (cdr l)))
      (else #f))))

(lat? '( atom (list))
      
 (atom? 'atom) -> #t
 (lat? '((list)))
 (atom? '(list)) -> #f
 #f

(lat? '(bacon (and eggs)))

(lat? '(car '(bacon (and eggs))))

(define l '(bacon (and eggs)))

(car l)

;;((atom? (car l)) (lat? (cdr l))) ;; it's an error

'( TRACING EXECUTION )
 (define ex '(atom1 atom2 (list)))
;; (lat? '(atom1 atom2 (list)))

 (null? ex) ;; #f
 (atom? (car ex) ) ;;#t , atom1 is an atom
 (cdr ex) ;; '(atom 2 (list))
      (lat? (cdr ex)) ;;recall the function lat?
           (null? (cdr ex)) ;; #f, the list is not empty
            (atom? (car (cdr ex))) ;;#t, atom2 is an atom
              (cdr(cdr ex))
            (lat? '((list))) ;;recall the function
              (null? '((list))) ;;#f
                (atom? (list)) ;;#f, '(list) is not an atom , is a list
                  #f ;; because the lat? function found in the exemple, the last argument being a list, not an atom



'member?
;; MEMBER?

(define member?
  (lambda ( a lat)
    (cond
      ((null? lat) #f )
      (else (or (eq? (car lat) a )
                (member? a (cdr lat)))))))

(member? 'bacon l) ;; true because te atom 'bacon is one of the atoms of l

(or (eq? (car l) 'bacon )
    (member? 'bacon (cdr l)))


'(TRACING EXECUTION)

(define bla '(b l a))
;; (member? 'a '(b l a))

(null? bla) ;; #f
  (eq? (car bla) 'a) ;;#f , (car bla)=b is not equal with a
(member? 'a '(l a))
   (null? '(l a)) ;;#f
      (eq? (car '(l a)) 'a) ;;#f, (car '(l a))=l is not equal with a
(member? 'a '(a))
      (null? '(a));;#f
          (eq? (car '(a)) 'a) ;;#t,because (car '(a))=a is equal with a
                              ;;--> means that 'a is a memeber of the list.

